from django.shortcuts import render

# Create your views here.
def about(request):
    return render(request, 'story3a.html')

def home(request):
    return render(request, 'story3h.html')

def profile(request):
    return render(request, 'story3p.html')